import { URLs } from './utils/Values';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as cors from "cors";
import AuthTokenRest from './controller/AuthTokenRest';
import CompanyRest from './controller/control-access/CompanyRest';
import UserRest from './controller/UserRest';
import ActionRest from './controller/control-access/ActionRest';
import BranchRest from './controller/control-access/BranchRest';
import ProfileRest from './controller/control-access/ProfileRest';

// Criando as configurações para o ExpressJS
class App {
// Instancia dele
  public exp: express.Application;
  constructor() {
    this.exp = express();
    this.middleware();
    this.routes();
  }
// Configuração para o nosso middler
  private middleware(): void {
    // this.exp.use(logger('dev'));
    this.exp.use(bodyParser.json());
    this.exp.use(bodyParser.urlencoded({ extended: true }));
    this.exp.use(cors());
  }
//Configuração da nossa API e nossos EndPoint e o famoso Hello 
  private routes(): void {   
    let router = express.Router();
  
    router.get('/', (req, res, next) => {
        console.log("TESTe");
      res.json({
        message: 'Hello World!'
      });
    });
    AuthTokenRest.setRoutes(this.exp);
    CompanyRest.setRoutes(this.exp);
    ActionRest.setRoutes(this.exp);
    BranchRest.setRoutes(this.exp);
    ProfileRest.setRoutes(this.exp);
    UserRest.setRoutes(this.exp);
  }
}

export default new App();