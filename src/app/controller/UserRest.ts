
import * as express from "express";
import { URLs } from "../utils/Values";
import UserService from "../service/UserService";

class UserRest{

    public setRoutes(express: express.Application){
        //Definindo as rotas.
        express.route(URLs.USER)
        .get(UserService.list)
        .post(UserService.create); 
        
        express.route(URLs.USER_ID)
        .get(UserService.getById)
        .delete(UserService.remove)
        .put(UserService.update);
    }
}
export default new UserRest();