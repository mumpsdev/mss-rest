
import * as express from "express";
import { URLs } from "../../utils/Values";
import ActionService from "../../service/control-access/ActionService";

class ActionRest{

    public setRoutes(express: express.Application){
        //Definindo as rotas.
        express.route(URLs.ACTION)
        .get(ActionService.list)
        .post(ActionService.create); 
        
        express.route(URLs.ACTION_ID)
        .get(ActionService.getById)
        .delete(ActionService.remove)
        .put(ActionService.update);
    }
}
export default new ActionRest();