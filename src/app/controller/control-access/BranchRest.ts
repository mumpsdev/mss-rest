
import * as express from "express";
import { URLs } from "../../utils/Values";
import BranchService from "../../service/control-access/BranchService";

class BranchRest{

    public setRoutes(express: express.Application){
        //Definindo as rotas.
        express.route(URLs.BRANCH)
        .get(BranchService.list)
        .post(BranchService.create); 
        
        express.route(URLs.BRANCH_ID)
        .get(BranchService.getById)
        .delete(BranchService.remove)
        .put(BranchService.update);
    }
}
export default new BranchRest();