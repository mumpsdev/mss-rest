
import * as express from "express";
import { URLs } from "../../utils/Values";
import CompanyService from "../../service/control-access/CompanyService";

class CompanyRest{

    public setRoutes(express: express.Application){
        //Definindo as rotas.
        express.route(URLs.COMPANY)
        .get(CompanyService.list)
        .post(CompanyService.create); 
        
        express.route(URLs.COMPANY_ID)
        .get(CompanyService.getById)
        .delete(CompanyService.remove)
        .put(CompanyService.update);
    }
}
export default new CompanyRest();