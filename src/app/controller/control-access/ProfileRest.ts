
import * as express from "express";
import { URLs } from "../../utils/Values";
import ProfileService from "../../service/control-access/ProfileService";

class ProfileRest{

    public setRoutes(express: express.Application){
        //Definindo as rotas.
        express.route(URLs.PROFILE)
        .get(ProfileService.list)
        .post(ProfileService.create); 
        
        express.route(URLs.PROFILE_ID)
        .get(ProfileService.getById)
        .delete(ProfileService.remove)
        .put(ProfileService.update);
    }
}
export default new ProfileRest();