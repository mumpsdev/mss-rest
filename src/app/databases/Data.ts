import * as mongoose from "mongoose";
import * as express from "express";
import Validation from "../utils/Validation";
import { typeMsg } from "../utils/Values";

export class Data{
    public obj: Object;
    public list: Array<any>;
    public listMsg: Array<any>;   
    public populates: Array<any>;
    
    public page: number;  
    public totalPages: number;  
    public limit: number;  
    public rangeInit: number;  
    public rangeEnd: number;  
    public asc: Array<any>;  
    public desc: Array<any>;  
    public fields: Array<any>;  
    public totalRows: number;  


    public addMsg(typeMsg: string, textMsg: string){
        if(!this.listMsg){
            this.listMsg = new Array();
        }
        this.listMsg.push({type: typeMsg, msg: textMsg});
    }

    public createFilter(request:express.Request): any{
        let filter = {};
        try {
            let obj = request.query;
            //Palavras que nâo entrarão no where.
            let notWhere: Array<string> = ["page", "limit", "asc", "desc", "fields", ];
            let LIMIT_PAGE = 10;
            // const SERVICO = Perfil.name.toLowerCase();   
            this.asc = obj.asc && obj.asc.indexOf(",") > -1 ? obj.asc.split(',') : obj.asc;
            this.desc = obj.desc && obj.desc.indexOf(",") > -1 ? obj.desc.split(',') : obj.desc;
            this.fields = obj.fields && obj.fields.indexOf(",") > -1 ? obj.fields.split(',') : obj.fields;
            this.page = obj.page ? obj.page : undefined;
            this.limit = (obj.limit || this.page) ? parseInt(obj.limit) || LIMIT_PAGE : undefined;
            let filter = this.createFilters(obj, notWhere);
            return filter;
        } catch (error) {
            console.log(error);
        }
        return filter;
    }

    public async executeQuery(query: mongoose.Query<any>){
        try {
            let sorting = this.getCriteriaOrders(this.asc, this.desc, "id");
            console.log(`Fields: ${this.fields}`);
            if(this.fields){
                query.select(this.fields);
            }
            if(this.page && this.limit){
                this.totalRows = await query.count();
                let skipe = (this.page-1) * this.limit;
                this.list = await query.skip(skipe).limit(this.limit).sort(sorting).exec("find");
                this.calcRanges();
            }else{
                this.list = await query.sort(sorting).exec("find");
            }
        } catch (error) {
            console.log(error);
        }
        this.populates = undefined;
        this.asc = undefined;
        this.desc = undefined;
        this.fields = undefined;
    }
    /**
     * Método que determina o critério de ordernação dos resultados
     * @param asc Recebe os nomes dos atributos que serão clasificados em ordem crescente
     * @param desc Recebe os nomes dos atributos que serão clasificados em ordem decrecente.
     * @param nomeCampoDefault Campo que será utilizado para classificar em ordem crescente caso não seja passado nem o ASC nem o DESC
     */
    public getCriteriaOrders(asc : Array<any>, desc : Array<any>, nomeCampoDefault: string) {
        let orderParams = [];
        if(asc && asc.length > 0) {
            asc = [].concat( asc);
            asc.forEach(element => {
                orderParams.push([element, 'ASC']);    
            });
        }
        if(desc && desc.length > 0) {
            desc = [].concat( desc);
            desc.forEach(element => {
                orderParams.push([element, 'DESC']);    
            });
        }
        if(orderParams.length == 0) {
            orderParams.push([nomeCampoDefault, 'ASC']);
        }
        return orderParams;
    }
    /**
     * Função que calcula a quantiade de dados que estão sendo mostrados na paginação dependendo dos seus filtros.
     */
    public calcRanges(){
        if(this.page > 0 && this.totalRows > 0){
            this.totalPages = Math.ceil(this.totalRows / this.limit);
        }
        if(this.page < this.totalPages){
            this.rangeEnd = this.page * this.limit;
        } else {
            this.rangeEnd = this.totalRows;
            this.page = this.totalPages;
        }
        if(this.page != this.totalPages){
            this.rangeInit = (this.rangeEnd + 1) - this.limit 
        } else{
            this.rangeInit =  ((this.totalPages - 1) * this.limit) + 1;

        }
    }
    /**
     * Pega todos os parametros que são passados pelo método get e criam ele em forma de atributos, que são filtrados na pesquisa.
     * @param object Normamente é o Query do Request.
     * @param notWhere São as palavras reservadas ultilizados no filtro que não poderão ser atributos do objeto.
     */
    public createFilters(object: Object, notWhere: Array<string>): any{
        console.log("Create");
        let obj = {};
        console.log(object);
        for (var key in object) {
            let value = object[key];
            let valueValid = value.replace(/\*/g, "");
            if(valueValid && notWhere.indexOf(key) < 0){
                console.log(value);
                this.setLike(value, obj, key);
            } 
        }
        return obj;
    }

    public setLike(value: string, obj: any, key: string){
        let likeInit: boolean = false, likeEnd: boolean = false;
        if(value.indexOf("*") == 0){
            value = value.substring(1);
            likeInit = true;
        }
        if(value.indexOf("*") == (value.length -1)){
            value = value.substring(0, value.length -1);
            likeEnd = true;
        }
        if(key.indexOf("_id") < 0 && (likeInit || likeEnd)){
            console.log(`Key: ${key}`);
            if(likeInit && !likeEnd){
                value = "(?i)^" + value + ".*";
            }else if(!likeInit && likeEnd){
                value = "(?i).*" + value + "$";
            }else{
                value = "(?i).*" + value + ".*";
            }
            obj[key]  = { $regex: value };
        }else{
            obj[key] = value;
        }
    }

    public getUrlPattern(versao: string, urlOriginal: string, isNoSql: boolean): string{
        let urlSemVersao = (urlOriginal.indexOf("?") > -1)
              ? urlOriginal.substring(versao.length + 1, urlOriginal.indexOf("?"))
              : urlOriginal.substring(versao.length + 1);
        let dominio = (urlSemVersao.indexOf("/") > -1) 
              ? urlSemVersao.substring(0, urlSemVersao.indexOf("/"))
              : urlSemVersao;
        let parametros = urlSemVersao.substring(dominio.length+1);
        let partes = parametros.split("/");
        parametros = "";
        if(partes && partes.length > 0){
            let valor = partes[0];
            if(valor && partes.length == 1){
                if(Validation.isOnlyNumbers(valor) || (isNoSql && Validation.containsNumbers(valor))){
                    parametros += "/:id"; 
                }else{
                    parametros += "/" + valor; 
                }
            }else{
                partes.forEach((p, index) =>{
                if(index > 0 && (index +1) % 2 == 0){
                    let campo = partes[index-1];
                    parametros +=  "/" + campo + "/:" + campo; 
                }
                });
          }
        }
        return versao + "/" + dominio + parametros;
    }

    public getActionsUser(usuario: any): Array<string>{
        let acoesUsuario: Array<string> = [];
        let profiles = usuario["profiles"];
        if(profiles){
            profiles.forEach(perfil => {
                let actions = perfil["branchActions"];
                if(actions){
                    actions.forEach(branchActions => {
                        let urlTypeBranch = "";
                        let action = branchActions["action"];
                        let branch  = branchActions["branch"]
                        if(action){
                            urlTypeBranch = action.url + "-" + action.type;
                        }
                        if(branch){
                            urlTypeBranch += "-" + branch._id;
                        }
                        if(urlTypeBranch &&  acoesUsuario.indexOf(urlTypeBranch) < 0){
                            acoesUsuario.push(urlTypeBranch);
                        }
                    });
                }
            });
        }
        return acoesUsuario;
    }

     /**
     * Função que verifica se existem alguma mensagem do tipo DANGER na lista de mensagem.
     */
    public isErrors(): boolean {
        if(this.listMsg && this.listMsg.length > 0){
            return this.listMsg.some( (msg) => msg.type && msg.type == typeMsg.DANGER);
        }
        return false;
    }
    /**
     * Verifica se a condição é verdadeira, caso não seja será adicionada uma mensagem com a mensagem passado por parâmetro.
     * @param validation 
     * @param msgError 
     */
    public isNotValidAddError(validation: any, msgError: string){
        if(!validation){
            this.addMsg(typeMsg.DANGER, msgError);
        }
    }
}