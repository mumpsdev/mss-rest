import * as mongoose from "mongoose";

class FactoryDb{
    constructor(){
        // mongoogse.Promise(global.Promise);
    }
    //mongodb://<dbuser>:<dbpassword>@ds125479.mlab.com:25479/mss_db
   

    private dbMongo: any;
    private connMongo: mongoose.Connection;
    private nodeEnv:string = process.env.NODE_ENV || "No environment";
    // dbmumps-shard-00-00-hm1rg.mongodb.net:27017
    public async createConnectionMongo(onConnected: any, onDisnnected: any, onError: any){
        let url: string = "mongodb+srv://";
        let host: string = "localhost";
        let user: string = "";
        let password = "";
        let port = "";
        let database = "dbmumps";
        let options: mongoose.ConnectionOptions;

        console.log("Node_ENV: " + this.nodeEnv);
        if(this.nodeEnv.indexOf("test") > -1){
            // mongodb+srv://mumps:<PASSWORD>@dbmumps-hm1rg.mongodb.net/test?retryWrites=true

        }else if(this.nodeEnv.indexOf("development") > -1){
            
            host = "@dbmumps-hm1rg.mongodb.net";
            port = "27017";
            user = "mumps";
            password = "987654321";
            options = {
                useNewUrlParser: true,
                reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
                reconnectInterval: 500, // Reconnect every 500ms
                poolSize: 10, // Maintain up to 10 socket connections
                // If not connected, return errors immediately rather than waiting for reconnect
                bufferMaxEntries: 0
            }
        }else if(this.nodeEnv.indexOf("production") > -1){

        }
        console.log("Running in environment: " + this.nodeEnv);
        let urlConnection = this.getUrlConnectionMongo(user, password, url, host, database, null);
        mongoose.set('debug', true);
        //Conectando ao banco
        try {
            let url = "mongodb+srv://mumps:987654321@dbmumps-hm1rg.mongodb.net/test?retryWrites=true"
            this.dbMongo = await mongoose.connect(urlConnection, options);
            //Conexao
            this.connMongo = await mongoose.connection;
    
            this.connMongo.on("connected", onConnected);
            
            this.connMongo.on("disconnected", onDisnnected);
            
            this.connMongo.on("error", onError);
        } catch (error) {
            console.log(error);
        }
    }

    public getUrlConnectionMongo(user: string, password: string, url: string, host: string, database: string, port: string): string{
        let userpassword = (user && password) ? user + ":" + password : "";
        port = (port) ? ":" + port : "";
        let urlConnection = url + userpassword + host + port + "/" + database + "?retryWrites=true";
        console.log(`UrL: ${urlConnection}`);
        return urlConnection;
    }

    public getConnectionMongo(): mongoose.Connection{
        return this.connMongo;
    }

    public closeConnectionMongo(onClose: any){
        if(this.connMongo){
            this.connMongo.close(onClose);
        }
    }
}
export default new FactoryDb();