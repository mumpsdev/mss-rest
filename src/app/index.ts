import * as http from 'http';
import * as debug from 'debug';
import App from './App';
import FactoryDB from './databases/FactoryDB';


debug('ts-express:server');

const port = normalizePort(process.env.PORT || 3000);

App.exp.set('port', port);

const server = http.createServer(App.exp);

server.listen(port, () =>{
    try {
        
        console.log("Servidor está rodando na porta "+ port);
        FactoryDB.createConnectionMongo(() =>{
            console.log("Database connected with success!");
        }, () => {
            console.log("database has been disconnected!");
        }, (error) => {
            console.log("Error trying to connect to database: " + error);
        });
    } catch (error) {
        console.log(error);
    }
});

server.on('error', onError);

server.on('listening', onListening);

process.once("SIGUSR2", () => {
    FactoryDB.closeConnectionMongo(() => {
         console.log("System reboot!");
         process.kill(process.pid, "SIGUSR2");
    });
});

process.once("SIGINT", () => {
    FactoryDB.closeConnectionMongo(() => {
       console.log("Sistema fechado!");
       process.exit(0);
    });
});

function normalizePort(val: number|string): number|string|boolean {
    let port: number = (typeof val === 'string') ? parseInt(val, 10) : val;
    if (isNaN(port)) return val;
    else if (port >= 0) return port;
    else return false;
}

function onError(error: NodeJS.ErrnoException): void {
  if (error.syscall !== 'listen') throw error;
  let bind = (typeof port === 'string') ? 'Pipe ' + port : 'Port ' + port;
  switch(error.code) {
    case 'EACCES':
      console.error(`${bind} requires elevated privileges`);
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(`${bind} is already in use`);
      process.exit(1);
      break;
    default:
      throw error;
  }
}

function onListening(): void {
  let addr = server.address();
  let bind = (typeof addr === 'string') ? `pipe ${addr}` : `port ${addr.port}`;
  debug(`Listening on ${bind}`);
  debug(`Listening on teste`);
}