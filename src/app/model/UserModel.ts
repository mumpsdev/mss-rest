import * as mongoose from "mongoose";

export let UserSchema = new mongoose.Schema({
    login: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        unique: true
    },
    name: {
        type: String,
        required: true,
    },
    token:{
        type: String
    },
    isOwner: {
        type: Boolean,
        default: false
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    profiles:[{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Profile"
    }]
});

export default mongoose.model("User", UserSchema);