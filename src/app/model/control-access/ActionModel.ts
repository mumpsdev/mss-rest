import * as mongoose from "mongoose";

export let ActionSchema = new mongoose.Schema({
    nome:{
        type: String,
        required: true,
        unique: true,
    },
    url:{
        type: String,
        required: true
    },
    metodo:{
        type: String
    },
    tipo:{
        type: String
    },
    rota:{
        type: String
    },
    dispositivos:{
        type: []
    },
    menu:{
        type: String
    },
    icon:{
        type: String
    }
});
export default mongoose.model("Action", ActionSchema);