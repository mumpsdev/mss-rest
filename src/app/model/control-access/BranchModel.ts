import * as mongoose from "mongoose";

export let BranchSchema = new mongoose.Schema({
    fantasy:{
        type: String,
        required: true,
    },
    socialReason:{
        type: String,
        unique: true,
        required: true,
    },
    company: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Company"
    }
});

export default mongoose.model("Branch", BranchSchema);