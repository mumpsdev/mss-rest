import * as mongoose from "mongoose";

export let CompanySchema = new mongoose.Schema({
    fantasy:{
        type: String,
        required: true,
    },
    socialReason:{
        type: String,
        unique: true,
        required: true,
    }
    , branchs: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Branch'}]

});

export default mongoose.model("Company", CompanySchema);