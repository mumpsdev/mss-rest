import * as mongoose from "mongoose";

export let ProfileSchema = new mongoose.Schema({
    name:{
        type: String,
        required: true,
        unique: true
    },
    
    branchActions:[{
        action: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Action"
        },
        branch: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Branch"
        }
    }]
});

export default mongoose.model("Profile", ProfileSchema);