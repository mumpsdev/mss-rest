import * as jwt from "jsonwebtoken";
import { Data } from "../databases/Data";
import UserModel from "../model/UserModel";
import { typeMsg, secretToken } from "../utils/Values";
import { msgGeneric, msgAuth } from "../utils/MessageTranslate";
import UtilService from "./UtilService";
import * as express from "express";

class AuthTokenService{
    constructor(){
    }

    public async getLoginAuth(req: express.Request, res: express.Response){
        let data = new Data();
        let login = req.params.login;
        try {
            data.obj = await UserModel.findOne({"login": login} , "login name");
            if(data.obj){
				res.status(200).json(data);
            }else{
                data.addMsg(typeMsg.DANGER, msgGeneric.recordNotExist);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorFindRecord);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }

    public async authenticate(req: express.Request, res: express.Response){
        let data = new Data()
        try {
            let login = req.body.login
            let password = req.body.password
            password = UtilService.encrypt(password);
            let result: {[k: string]: any} = {}
            result.user = await UserModel.findOne({"login": login, "password": password}, "login name isOwner isAdmin")
            .populate({
                path: "profiles",
                populate: [
                    {path: "action"}, {path: "branch"}
                ]
            });
            console.log(result.user);
            
            data.isNotValidAddError(result.user, msgAuth.loginPasswordInvalid);
            if(data.isErrors()){
                return res.status(401).json(data);
            }
            result.user['password'] = '';

            let actions = data.getActionsUser(result.user);
            let dataBase = Date.now();

            let token = jwt.sign(
                {   "iss" : 'mss', // (Issuer) Origem do token
                    "iat": Math.floor(dataBase), // (issueAt) Timestamp de quando o token foi gerado
                    "exp" : Math.floor(dataBase + ( 1 * 60 * 60 * 1000 ) ), //(Expiration) Timestamp de quando o token expira
                    "sub" : result.user._id, //(Subject) Entidade a quem o token pertence
                    "login": result.user["login"],
                    "actions": actions
                }, secretToken.SECRET);

            result[secretToken.TOKEN] = token;
            data.obj = result;
            res.status(200).json(data);
        } catch (error) {
            console.log(error);
            data.addMsg(typeMsg.DANGER, msgAuth.errorQueryLoginAccess)
            res.status(401).json(data)
        }
    }

    public async validateToken(req: express.Request, res: express.Response, next: express.NextFunction){
        let data = new Data()
        try {
            let token = req.body[secretToken.TOKEN] || req.query[secretToken.TOKEN] || req.headers[secretToken.TOKEN];
            if(token){
                let userLogged = jwt.verify(token, secretToken.SECRET)
                let actions: Array<string> = userLogged["actions"];
                let urlOriginal = data.getUrlPattern("/api/v1", req.originalUrl, true);
                let urlTypeBranch = urlOriginal + "-" + req.method;
                let branchId = req.body.branchId;
                if(branchId){
                    urlTypeBranch += "-" + branchId;
                }
                if(!actions || actions.indexOf(urlTypeBranch) < 0){
                    data.addMsg(typeMsg.DANGER, msgAuth.acessDanied);
                    return res.status(401).json(data);
                }
                next();
            }else{
                data.addMsg(typeMsg.DANGER, msgAuth.notSendToken);
                res.status(401).json(data);
            }
        } catch (error) {
            console.log(error);
            data.addMsg(typeMsg.DANGER, msgAuth.errorValidToken);
            res.status(500).json(data);
            return
        }    
    }
}

export default new AuthTokenService();