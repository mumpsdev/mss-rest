
import { Data } from "../databases/Data";
import { msgGeneric } from "../utils/MessageTranslate";
import { typeMsg } from "../utils/Values";
import UserModel from "../model/UserModel";
import  * as express from "express";
import FactoryDB from "../databases/FactoryDB";
import UtilService from "./UtilService";

class UserService{
    public async list(req: express.Request, res: express.Response){
        let data = new Data();
        try {
            // let conn = FactoryDB.getConnectionMongo();
            // let client = await conn.startSession();
            // client.startTransaction();
            let filter = data.createFilter(req);
            console.log("Filter");
            console.log(filter);
            let query = UserModel.find(filter);
            query.populate({
                path: "branchActions", 
                populate: [
                    {path: "action"},
                    {path: "branch"}
                ]
            });
            await data.executeQuery(query);
            res.status(200).json(data);
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorListRecords);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }

    public async create(req: express.Request, res: express.Response){
        let data = new Data();
        try {
            let user = req.body;
            data.isNotValidAddError(user.name, msgGeneric.notSetField)
            data.isNotValidAddError(user.login, msgGeneric.notSetField)
            data.isNotValidAddError(user.password, msgGeneric.notSetField)
            if(data.isErrors()){
                return  res.status(500).json(data);
            }
            user.password = UtilService.encrypt(user.password);
            data.obj = await UserModel.create(user);
            data.addMsg(typeMsg.SUCCESS, msgGeneric.recordInsertedSuccessfully);
            res.status(200).json(data);
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorInsertRecord);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }

    public async update(req: express.Request, res: express.Response){
        let data = new Data();
        let id = req.params.id;
        try {
            data.obj = await UserModel.findByIdAndUpdate(id, req.body, {new: true});
            if(data.obj){
                data.addMsg(typeMsg.SUCCESS, msgGeneric.recordAlteredSuccessfully);
                res.status(200).json(data);
            }else{
                data.addMsg(typeMsg.DANGER, msgGeneric.recordNotExist);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorAlterRecord);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }

    public async remove(req: express.Request, res: express.Response){
        let data = new Data();
        let id = req.params.id;
        try {
            data.obj = await UserModel.findByIdAndRemove(id);
            if(data.obj){
                data.addMsg(typeMsg.SUCCESS, msgGeneric.recordRemovedSuccessfully);
                res.status(200).json(data);
            }else{
                data.addMsg(typeMsg.DANGER, msgGeneric.recordNotExist);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorRemoveRecord);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }

    public async getById(req: express.Request, res: express.Response){
        let data = new Data();
        let id = req.params.id;
        console.log(id);
        try {
            data.obj = await UserModel.findById(id)
            .populate({
                path: "profiles",
                populate: [
                    {path: "action"}, {path: "branch"}
                ]
            });
            if(data.obj){
                res.status(200).json(data);
            }else{
                data.addMsg(typeMsg.DANGER, msgGeneric.notRecordsFound);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorFindRecord);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }
       
}

export default new UserService();