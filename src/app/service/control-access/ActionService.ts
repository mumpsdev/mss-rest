import * as express from "express";
import { Data } from "../../databases/Data";
import ActionModel from "../../model/control-access/ActionModel";
import { typeMsg } from "../../utils/Values";
import { msgGeneric } from "../../utils/MessageTranslate";

class AcaoService{

    public async list(req: express.Request, res: express.Response){
        let data = new Data();
 
        try {
            let filter = data.createFilter(req);
            let query = ActionModel.find(filter);
            await data.executeQuery(query);
            res.status(200).json(data);
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorListRecords);
            console.log("Erro: " + error);
            res.status(500).json(data);

        }
    }

    public async create(req: express.Request, res: express.Response){
        let data = new Data();
        try {
            data.obj = await ActionModel.create(req.body)
            data.addMsg(typeMsg.SUCCESS, msgGeneric.recordInsertedSuccessfully);
            res.status(200).json(data);
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorInsertRecord);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }

    public async update(req: express.Request, res: express.Response){
        let data = new Data();
        let id = req.params.id;
        try {
            data.obj = await ActionModel.findByIdAndUpdate(id, req.body, {new: true});
            if(data.obj){
                data.addMsg(typeMsg.SUCCESS, msgGeneric.recordInsertedSuccessfully);
                res.status(200).json(data);
            }else{
                data.addMsg(typeMsg.DANGER, msgGeneric.recordNotExist);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorAlterRecord);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }

    public async remove(req: express.Request, res: express.Response){
        let data = new Data();
        let id = req.params.id;
        try {
            data.obj = await ActionModel.findByIdAndRemove(id);
            if(data.obj){
                data.addMsg(typeMsg.SUCCESS, msgGeneric.recordRemovedSuccessfully);
                res.status(200).json(data);
            }else{
                data.addMsg(typeMsg.DANGER, msgGeneric.recordNotExist);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorRemoveRecord);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }

    public async getById(req: express.Request, res: express.Response){
        let data = new Data();
        let id = req.params.id;
        try {
            data.obj = await ActionModel.findById(id);
            if(data.obj){
                res.status(200).json(data);
            }else{
                data.addMsg(typeMsg.DANGER, msgGeneric.recordNotExist);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorFindRecord);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }
}

export default new AcaoService();