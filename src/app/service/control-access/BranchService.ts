import * as express from "express";
import { ClientSession } from "mongoose";
import { Data } from "../../databases/Data";
import BranchModel from "../../model/control-access/branchModel";
import { msgGeneric } from "../../utils/MessageTranslate";
import { typeMsg } from "../../utils/Values";

class FilialService{

    public async list(req: express.Request, res: express.Response){
        let data = new Data();
        try {
            let filter = data.createFilter(req);
            let query = BranchModel.find(filter);
            query.populate("organizacao", "fantasia");
            await data.executeQuery(query);
            res.status(200).json(data);
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorListRecords);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }

    public async create(req: express.Request, res: express.Response){
        let data = new Data();
        let session: ClientSession;
        try {
            session = await BranchModel.db.startSession();
            await session.startTransaction();
            data.obj = await BranchModel.create(req.body)
            data.addMsg(typeMsg.DANGER, msgGeneric.recordInsertedSuccessfully);
            res.status(200).json(data);
            await session.commitTransaction();
        } catch (error) {
            console.log("Erro: " + error);
            await session.abortTransaction();
            data.addMsg(typeMsg.DANGER, msgGeneric.errorInsertRecord);
            res.status(500).json(data);
        }finally{
            await session.endSession();
        }
    }

    public async update(req: express.Request, res: express.Response){
        let data = new Data();
        let id = req.params.id;
        let session: ClientSession;
        try {
            session =  await BranchModel.db.startSession();
            await session.startTransaction();
            data.obj = await BranchModel.findByIdAndUpdate(id, req.body, {new: true});
            if(data.obj){
                data.addMsg(typeMsg.SUCCESS, msgGeneric.recordAlteredSuccessfully);
                res.status(200).json(data);
            }else{
                data.addMsg(typeMsg.DANGER, msgGeneric.recordNotExist);
                res.status(404).json(data);
            }
            await session.commitTransaction();
        } catch (error) {
            console.log("Erro: " + error);
            await session.abortTransaction();
            data.addMsg(typeMsg.DANGER, msgGeneric.errorAlterRecord);
            res.status(500).json(data);
        }finally{
            session.endSession();
        }
    }

    public async remove(req: express.Request, res: express.Response){
        let data = new Data();
        let id = req.params.id;
        let session: ClientSession
        try {
            session = await BranchModel.db.startSession();
            await session.startTransaction();
            data.obj = await BranchModel.findByIdAndRemove(id);
            if(data.obj){
                data.addMsg(typeMsg.SUCCESS, msgGeneric.recordRemovedSuccessfully);
                res.status(200).json(data);
            }else{
                data.addMsg(typeMsg.DANGER, msgGeneric.recordNotExist);
                res.status(404).json(data);
            }
            await session.commitTransaction();
        } catch (error) {
            console.log("Erro: " + error);
            await session.abortTransaction();
            data.addMsg(typeMsg.DANGER, msgGeneric.errorRemoveRecord);
            res.status(500).json(data);
        }finally{
            session.endSession();
        }
    }

    public async getById(req: express.Request, res: express.Response){
        let data = new Data();
        let id = req.params.id;
        try {
            data.obj = await BranchModel.findById(id).populate("company");
            if(data.obj){
                res.status(200).json(data);
            }else{
                data.addMsg(typeMsg.DANGER, msgGeneric.recordNotExist);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorFindRecord);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }
}

export default new FilialService();