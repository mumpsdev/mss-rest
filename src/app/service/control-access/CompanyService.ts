import * as express from "express";

import { Data } from "../../databases/Data";
import CompanyModel from "../../model/control-access/CompanyModel";
import { typeMsg } from "../../utils/Values";
import { msgGeneric } from "../../utils/MessageTranslate";
import { ClientSession, Mongoose } from "mongoose";

class OrganizacaoService{
    public async list(req, res){
        let data = new Data();
        let session: ClientSession;
        try {
            session = await CompanyModel.db.startSession();
            
            await session.startTransaction();
            let filter = data.createFilter(req);
            let query = CompanyModel.find(filter);
            query.populate("branchs");
            await data.executeQuery(query);
            res.status(200).json(data);
            await session.commitTransaction();
        } catch (error) {
            await session.abortTransaction();
            data.addMsg(typeMsg.DANGER, msgGeneric.errorListRecords);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }finally {
            await session.endSession();
        }
    }
    
    public async create(req: express.Request, res: express.Response){
        let data = new Data();
        let session: ClientSession;
        try {
            session = await CompanyModel.db.startSession();
            await session.startTransaction();
            data.obj = await CompanyModel.create(req.body)
            data.addMsg(typeMsg.SUCCESS, msgGeneric.recordInsertedSuccessfully);
            res.status(200).json(data);
            await session.commitTransaction();
        } catch (error) {
            console.log("Erro: " + error);
            await session.abortTransaction();
            data.addMsg(typeMsg.DANGER, msgGeneric.errorInsertRecord);
            res.status(500).json(data);
        }finally{
            await session.endSession();
        }
    }
    
    public async update(req: express.Request, res: express.Response){
        let data = new Data();
        let id = req.params.id;
        let session: ClientSession;
        try {
            session = await CompanyModel.db.startSession();
            await session.startTransaction();
            data.obj = await CompanyModel.findByIdAndUpdate(id, req.body, {new: true});
            if(data.obj){
                data.addMsg(typeMsg.SUCCESS, msgGeneric.recordAlteredSuccessfully);
                res.status(200).json(data);
            }else{
                data.addMsg(typeMsg.DANGER, msgGeneric.recordNotExist);
                res.status(404).json(data);
            }
            await session.commitTransaction();
        } catch (error) {
            console.log("Erro: " + error);
            await session.abortTransaction();
            data.addMsg(typeMsg.DANGER, msgGeneric.errorAlterRecord);
            res.status(500).json(data);
        }finally{
            await session.endSession();
        }
    }
    
    public async remove(req: express.Request, res: express.Response){
        let data = new Data();
        let id = req.params.id;
        let session: ClientSession;
        try {
            session = await CompanyModel.db.startSession();
            await session.startTransaction();
            data.obj = await CompanyModel.findByIdAndRemove(id);
            if(data.obj){
                data.addMsg(typeMsg.SUCCESS, msgGeneric.recordRemovedSuccessfully);
                res.status(200).json(data);
            }else{
                data.addMsg(typeMsg.DANGER, msgGeneric.recordNotExist);
                res.status(404).json(data);
            }
            await session.commitTransaction();
        } catch (error) {
            console.log("Erro: " + error);
            await session.abortTransaction();
            data.addMsg(typeMsg.DANGER, msgGeneric.errorRemoveRecord);
            res.status(500).json(data);
        }finally{
            await session.endSession();
        }
    }

    public async getById(req: express.Request, res: express.Response){
        let data = new Data();
        let id = req.params.id;
        try {
            let query = CompanyModel.findById(id);
            query.populate("branchs");
            data.obj = await query.exec("find");

            if(data.obj){
                res.status(200).json(data);
            }else{
                data.addMsg(typeMsg.DANGER, msgGeneric.recordNotExist);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorFindRecord);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }
}

export default new OrganizacaoService();