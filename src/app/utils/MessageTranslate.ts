export const msgGeneric = {
    "notRecordsFound"           : "NOT_RECORDS_FOUND",
    "errorFindRecord"           : "ERROR_FIND_RECORD",
    "errorListRecords"          : "ERROR_LIST_RECORDS",
    "errorInsertRecord"         : "ERROR_INSERT_RECORD",
    "errorAlterRecord"          : "ERROR_ALTER_RECORD",
    "errorRemoveRecord"          : "ERROR_REMOVE_RECORD",
    "recordInsertedSuccessfully": "RECORD_INSERTED_SUCCESSFULLY",
    "recordAlteredSuccessfully" : "RECORD_ALTERED_SUCCESSFULLY",
    "recordRemovedSuccessfully" : "RECORD_REMOVED_SUCCESSFULLY",
    "notSetField"               : "NOT_SET_FIELD",
    "recordAlreadyExists"       : "RECORD_ALREADY_EXISTS",
    "recordNotExist"            : "RECORD_NOT_EXIST"
}

export const msgAuth = {//Mensagens de retornos
    "errorQueryLoginAccess" : "ERROR_QUERY_LOGIN_ACCESS",
    "notSendToken"              : "NOT_SEND_TOKEN",
    "loginInvalid"                : "LOGIN_INVALID",
    "tokenInvalid"                : "TOKEN_INVALID",
    "loginPasswordInvalid"      : "LOGIN_PASSWORD_INVALID",
    "errorValidToken": "ERROR_VALID_TOKEN",
    "notLoginFound": "NOT_LOGIN_FOUND",
    "acessDanied" : "ACESS_DANIED",
    "tokenGenerateSuccessfully": "TOKEN_GENERATE_SUCCESSFULLY"
};
