export const URLs = {
    //---------------- Auth
    "AUTH_LOGIN": "/api/v1/login-auth/:login",
    "AUTH_AUTHENTICATE": "/api/v1/authenticate",
    "AUTH_ALL": "/*", 
    //---------------- Company
    "USER": "/api/v1/user",
    "USER_ID": "/api/v1/user/:id",
    //---------------- Company
    "COMPANY": "/api/v1/company",
    "COMPANY_ID": "/api/v1/company/:id",
    //---------------- Branch
    "BRANCH": "/api/v1/branch",
    "BRANCH_ID": "/api/v1/branch/:id",
    //---------------- Profile
    "PROFILE": "/api/v1/prifile",
    "PROFILE_ID": "/api/v1/profile/:id",
    //---------------- Action
    "ACTION": "/api/v1/action",
    "ACTION_ID": "/api/v1/action/:id"
}

export const typeMsg = {//Tipos de mensagens
    "DANGER": "msgErro",
    "SUCCESS": "msgSuccesso",
    "INFO": "msgInfo",
    "ALERT": "msgAlert"
}

export const objectMsg = {//Objetos de retorno
    "OBJ": "obj",
    "LIST": "list",
    "LIST_MSG": "listMsg",
    "PAGE": "page",
    "TOTAL_PAGES": "totalPages",
    "LIMIT": "limit",
    "RANGE_INIT": "rangeInit",
    "RANGE_END": "rangeEnd",
    "ASC": "asc",
    "DESC": "desc",
    "TOTAL_ROWS": "totalRows",
}

export const secretToken ={
    "TOKEN": "x-access-token",
    "TIME": 84600,
    "SECRET": "safra-system-rest"
}
